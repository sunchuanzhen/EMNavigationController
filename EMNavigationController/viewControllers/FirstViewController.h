//
//  FirstViewController.h
//  EMultiNavigation
//
//  Created by EasonWang on 13-11-5.
//  Copyright (c) 2013年 EasonWang. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SecondViewController.h"
#import "EMViewController.h"


@interface FirstViewController : EMViewController
{

    IBOutlet UIImageView *_imageView;
    IBOutlet UILabel *_labelText;
}

-(IBAction)clickButton:(id)sender;

@end
