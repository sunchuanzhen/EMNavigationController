#EMNavigationController

EMNavigationController致力于开发一套可实现快速搭建iOS项目的工程框架
，包含了自定义的navigationBar，自定义的tabbar
，已做了iOS6、iOS7的适配（为保证两个系统中的坐标系一致，请取消xib文件中Use Autolayout选项）

<br/>此框架实现手势返回操作，及手势从左向右滑动执行pop操作，返回上一级VC

<br/>注：详细部署情况请参阅wiki页面中的详细步骤

<h2>感谢 @曾宪华IT 提供的侧边栏菜单解决方案，在随后的版本更新中将嵌入XHDrawerController框架(http://git.oschina.net/iamking/XHDrawerController.git)，V2.0新版本中将集成底部菜单与侧边栏菜单两种开发框架，敬请期待...</h2>